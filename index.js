var pdfFiller   = require('pdffiller');
 
var sourcePDF = "test/test.pdf";
var destinationPDF =  "test/test_complete.pdf";
var data = {
    "name" : "John",
};
 
pdfFiller.fillForm( sourcePDF, destinationPDF, data, function(err) {
    if (err) throw err;
    console.log("In callback (we're done).");
});